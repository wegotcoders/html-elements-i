# Recipe Page #

Using HTML to create a recipe page.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open recipe.html in Sublime and have a look at its structure.
2. Choose to create your own recipe page or copy one from the internet.
3. Add a title for your recipe page to the <head> of recipe.html.
4. Add an image to the <header> section, image styling is in style.css file.
5. Use <h1>, <h2> and <h3> tags for the headings and sub-headings of your page.
6. Use an unordered list for the ingredients of your recipe.
7. Use an ordered list for the instructions of your recipe.
8. Add hyperlinks to your ingredients, pointing them to places to buy ingredients.
9. You can see your progress by typing in terminal 'open recipe.html'.